package mark.selenium;

import org.openqa.selenium.Capabilities;

/**
 * Describes the set of Capabilities used to create an instance of the WebDriver implementation,
 * merge two Capabilities together and/or creates a new instance of the WebDriver implementation.
 * 
 * @author melliott
 * @version 1.0
 */

public class CapabilityFactory {
    public Capabilities capabilities;
    public Capabilities getCapabilities (String browser) {
        if (browser.equals("firefox")) {
            capabilities=OptionsManager.getFirefoxOptions();
        }
        else
            capabilities=OptionsManager.getChromeOptions();
        return capabilities;
    }
}
