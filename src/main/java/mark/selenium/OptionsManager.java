package mark.selenium;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.Platform;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *  DO NOT REMOVE commented Java Package Imports! Uncomment them as needed.
 *  
 * Currently used to manage web browser options for cross browser testing. Also used to set path
 * of Firefox binaries. Will be deprecated in a future release when a WebDriverManager is integrated.
 * 
 * @author melliott
 * @version 1.0
 *
 */

public class OptionsManager {

    //Get Chrome Options
    public static ChromeOptions getChromeOptions() {
        ChromeOptions options=new ChromeOptions();
        //options.addArguments("--start-maximized");
        options.addArguments("--headless");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");
        //options.addArguments("--incognito");
        return options;
    }

    //Get Firefox Options
    public static FirefoxOptions getFirefoxOptions () {
        FirefoxOptions options=new FirefoxOptions();

        //options for running locally
        //String strFFBinaryPath="C:\\Users\\melliott\\AppData\\Local\\Mozilla Firefox\\firefox.exe";
        //options.setBinary(strFFBinaryPath);

        //global options
		options.addArguments("--headless");
		//options.addArguments("--disable-dev-shm-usage");
		//options.addArguments("--no-sandbox"); // Bypass OS security model
        //options.addPreference("dom.disable_beforeunload", true);
        //FirefoxProfile profile = new FirefoxProfile();
        //Accept Untrusted Certificates
        //profile.setAcceptUntrustedCertificates(true);
        //profile.setAssumeUntrustedCertificateIssuer(false);
        return options;
    }
}