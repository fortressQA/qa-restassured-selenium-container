package mark.selenium;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import io.restassured.RestAssured;

/**
 * Test runner for new RestAssured API tests. Utilizes testng.xml for running tests in parallel threads.
 * 
 * @author melliott
 * @version 1.0
 *
 */

public class RestAssuredAPITestsNew extends BaseTest {

	// Sample Application URL
	String apiURL="http://demo.guru99.com/V4/sinkministatement.php";
	String apiCID="68195";
	String apiPWD="1234!";
	String apiACCNUM="1";
	
	/**
	 * getAPIResquest: Sample API test utilizing customer id, password and account number. Verify correct response received.
	 */

	@Test
	public void getAPIRequest() {
		System.out.println("TEST: [restassured]" + "\tgetAPIRequest: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		int code=RestAssured.given().queryParam("CUSTOMER_ID", apiCID)
				.queryParam("PASSWORD", apiPWD)
				.queryParam("Account_No", apiACCNUM)
				.when().get(apiURL).statusCode();
		Assert.assertEquals(code, 200,"Expecting status code 200");
		System.out.println("TEST: [restassured]" + "\tgetAPIRequest: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [restassured]" + "\tgetAPIRequest: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}
	
	/**
	 * getAPIResponseBody: Sample API test utilizing customer id, password and account number. Print body and verify response. 
	 */

	@Test
	public void getResponseBody() {
		System.out.println("TEST: [restassured]" + "\tgetAPIResponseBody: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		RestAssured.given().queryParam("CUSTOMER_ID", apiCID)
		   .queryParam("PASSWORD", apiPWD)
		   .queryParam("Account_No", apiACCNUM)
		   .when().get(apiURL).then().log().body().statusCode(200); //remove .log().body() if you don't want to print the message body
		System.out.println("TEST: [restassured]" + "\tgetResponseBody: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [restassured]" + "\tgetResponseBody: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}
}