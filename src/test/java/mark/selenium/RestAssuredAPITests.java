package mark.selenium;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import io.restassured.RestAssured;

/**
 * Test runner for all RestAssured API tests. Utilizes testng.xml for running tests in parallel threads.
 * 
 * @author melliott
 * @version 1.0
 *
 */

public class RestAssuredAPITests extends BaseTest {

    // Sample Application URL
	String apiURL="http://api.postcodes.io/postcodes/OX495NU";

	@Test
	public void getAPIRequest() {
		System.out.println("TEST: [restassured]" + "\tgetAPIRequest: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		int code=RestAssured.get(apiURL).getStatusCode();
		Assert.assertEquals(code, 200,"Status code is not comming as 200");
		System.out.println("TEST: [restassured]" + "\tgetAPIRequest: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [restassured]" + "\tgetAPIRequest: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}
}