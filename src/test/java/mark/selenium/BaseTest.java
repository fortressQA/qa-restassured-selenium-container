package mark.selenium;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

/**
 * BaseTest is used to setup Selenium's webdriver (local and remote) for cross browser testing. Used by test runners and 
 * CapabilityFactory.java.
 * 
 * @author melliott
 * @version 1.0
 *
 */

public class BaseTest {
	//Declare ThreadLocal Driver (ThreadLocalMap) for ThreadSafe Tests
	protected static ThreadLocal<RemoteWebDriver> driver=new ThreadLocal<>();
	public CapabilityFactory capabilityFactory=new CapabilityFactory();

	@BeforeMethod
	@Parameters(value={"browser"})

	public void setup (String browser1) throws MalformedURLException {
		String browser=System.getProperty("browser");
		String webdriverLocal="http://localhost:4444/wd/hub";
		String webdriverFF="http://selenium__standalone-firefox:4444/wd/hub";
		String webdriverGC="http://selenium__standalone-chrome:4444/wd/hub";

		//Uncomment to run on localhost
		//driver.set(new RemoteWebDriver(new URL(webdriverLocal), capabilityFactory.getCapabilities(browser)));

		//Uncomment to run on Gitlab
		if (browser.equalsIgnoreCase("firefox")) {
			driver.set(new RemoteWebDriver(new URL(webdriverFF), capabilityFactory.getCapabilities(browser)));
		}
		else //chrome
			driver.set(new RemoteWebDriver(new URL(webdriverGC), capabilityFactory.getCapabilities(browser)));
	}

	public WebDriver getDriver() {
		//Get driver from ThreadLocalMap
		return driver.get();
	}

	@AfterMethod
	public void tearDown() {
		getDriver().quit();
	}
}