package mark.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

/**
 * Test runner for all Selenium Firefox tests. Utilizes testng.xml for running tests in parallel threads.
 * 
 * @author melliott
 * @version 1.0
 *
 */

public class SeleniumFirefoxTests extends BaseTest {

	/**
	 * GoogleHomePage2: Navigate to Google.com then verify URL
	 */

	@Test
	public void GoogleHomePage2() {
		System.out.println("TEST: [firefox]" + "\tGoogleHomePage2: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		getDriver().navigate().to("http://www.google.com");
		//System.out.println("TEST: [firefox]" + "\tGoogleHomePage2: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " +  Thread.currentThread().getId());
		Assert.assertEquals(getDriver().getTitle(), "Google");
		System.out.println("TEST: [firefox]" + "\tGoogleHomePage2: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [firefox]" + "\tGoogleHomePage2: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}

	/**
	 * FortressHomePage2: Navigate to Fortressinfosec.com then verify URL
	 */

	@Test
	public void FortressHomePage2() {
		System.out.println("TEST: [firefox]" + "\tFortressHomePage2: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		getDriver().navigate().to("https://fortressinfosec.com/");
		//System.out.println("TEST: [firefox]" + "\tFortressHomePage2: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " + Thread.currentThread().getId());
		Assert.assertEquals(getDriver().getTitle(), "Fortress Information Security | Fortress Information Security");
		System.out.println("TEST: [firefox]" + "\tFortressHomePage2: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [firefox]" + "\tFortressHomePage2: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}

	/**
	 * FortressHomeCareerPage2: Navigate to Fortressinfosec.com, find and click /careers link then verify URL
	 */

	@Test
	public void FortressCareerPage2() throws InterruptedException {
		FortressHomePage2();
		System.out.println("TEST: [firefox]" + "\tFortressCareerPage2: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		getDriver().findElement(By.cssSelector("[href=\"/careers\"][class=\"none nav-link\"]")).click();
		Thread.sleep(2000);
		//System.out.println("TEST: [firefox]" + "\tFortressCareerPage2: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " + Thread.currentThread().getId());
		Assert.assertEquals(getDriver().getTitle(), "Careers | Fortress Information Security");
		System.out.println("TEST: [firefox]" + "\tFortressCareerPage2: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [firefox]" + "\tFortressCareerPage2: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}

	/**
	 * FortressHomeJobsPage2: Navigate to Fortressinfosec.com/careers, find and click /jobs link then verify URL
	 */

	@Test
	public void FortressJobsPage2() throws InterruptedException {
		FortressCareerPage2();
		System.out.println("TEST: [firefox]" + "\tFortressJobsPage2: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		getDriver().findElement(By.cssSelector("#rcc-confirm-button")).click();
		getDriver().findElement(By.xpath("(//a[text()='Click here to join the Team'])[1]")).click();
		Thread.sleep(2000);
		//System.out.println("TEST: [firefox]" + "\tFortressJobsPage: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " + Thread.currentThread().getId());
		Assert.assertEquals(getDriver().getTitle(), "Job Search | Fortress Information Security");
		System.out.println("TEST: [firefox]" + "\tFortressJobsPage2: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [firefox]" + "\tFortressJobsPage1: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}

	/**
	 * JWatchLogInTest2: DEPENDS_ON - Navigate to https://www.jwatch.org/user/login, find and fill login username, 
	 * find and fill login password, find and click on sign-in button then verify correct URL
	 */

	@Test
	public void JWatchLogInTest2() {
		System.out.println("TEST: [firefox] " + "\tJWatchLogInTest2: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		getDriver().navigate().to("https://www.jwatch.org/user/login");
		getDriver().findElement(By.cssSelector(".main-content #email_text")).sendKeys("neo1491@msn.com");
		getDriver().findElement(By.cssSelector(".main-content #pwd_text")).sendKeys("FortressQA");
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("$('.main-content .btn-wrapper')[0].click()");
		//System.out.println("TEST: [firefox] " + "\tJWatchLogInTest2: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " +  Thread.currentThread().getId());
		Assert.assertEquals(getDriver().getTitle(), "NEJM Journal Watch");
		System.out.println("TEST: [firefox] " + "\tJWatchLogInTest2: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [firefox] " + "\tJWatchLogInTest2: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}

	/**
	 * DisneyWorldLogInTest2: DEPENDS_ON - Navigate to https://disneyworld.disney.go.com/login, find and fill login username, 
	 * find and fill login password, find and click on sign-in button then verify correct URL
	 */

	@Test
	public void DisneyWorldLogInTest2() {
		System.out.println("TEST: [firefox] " + "\tDisneyWorldLogInTest2: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
		getDriver().navigate().to("https://disneyworld.disney.go.com/login");
		getDriver().findElement(By.cssSelector("[type='email']")).sendKeys("neo1491@msn.com");
		getDriver().findElement(By.cssSelector("[type='password']")).sendKeys("Fortre$$QA");
//        executor.executeScript("$('.main-content .btn-wrapper')[0].click()");
		getDriver().findElement(By.cssSelector("[type='submit']")).click();
		System.out.println("TEST: [firefox] " + "\tDisneyWorldLogInTest2: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " +  Thread.currentThread().getId());
		Assert.assertEquals(getDriver().getTitle(), "Walt Disney World Resort in Orlando, Florida");
		System.out.println("TEST: [firefox] " + "\tDisneyWorldLogInTest2: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
		Reporter.log("TEST: [firefox] " + "\tDisneyWorldLogInTest2: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
	}
}