package mark.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;

/**
 * Test runner for all Selenium Chrome tests. Utilizes testng.xml for running tests in parallel threads.
 * 
 * @author melliott
 * @version 1.0
 *
 */

public class SeleniumChromeTests extends BaseTest {

	/**
	 * GoogleHomePage1: Navigate to Google.com then verify URL
	 */

    @Test
    public void GoogleHomePage1() {
        System.out.println("TEST: [chrome] " + "\tGoogleHomePage1: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
        getDriver().navigate().to("http://www.google.com");
        //System.out.println("TEST: [chrome] " + "\tGoogleHomePage1: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " +  Thread.currentThread().getId());
        Assert.assertEquals(getDriver().getTitle(), "Google");
        System.out.println("TEST: [chrome] " + "\tGoogleHomePage1: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
        Reporter.log("TEST: [chrome] " + "\tGoogleHomePage1: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
    }

	/**
	 * FortressHomePage1: Navigate to Fortressinfosec.com then verify URL
	 */

    @Test
    public void FortressHomePage1() {
        System.out.println("TEST: [chrome] " + "\tFortressHomePage1: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
        getDriver().navigate().to("https://fortressinfosec.com/");
        //System.out.println("TEST: [chrome] " + "\tFortressHomePage1: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " + Thread.currentThread().getId());
        Assert.assertEquals(getDriver().getTitle(), "Fortress Information Security | Fortress Information Security");
        System.out.println("TEST: [chrome] " + "\tFortressHomePage1: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
        Reporter.log("TEST: [chrome] " + "\tFortressHomePage1: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
    }

	/**
	 * FortressHomeCareerPage1: DEPENDS_ON - Navigate to Fortressinfosec.com, find and click /careers link then verify URL.
	 */

    @Test
    public void FortressCareerPage1() throws InterruptedException {
    	FortressHomePage1(); //go to homepage
        System.out.println("TEST: [chrome] " + "\tFortressCareerPage1: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
        getDriver().findElement(By.cssSelector("[href=\"/careers\"][class=\"none nav-link\"]")).click(); //click on career link
        Thread.sleep(2000);
        //System.out.println("TEST: [chrome] " + "\tFortressCareerPage1: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " + Thread.currentThread().getId());
        Assert.assertEquals(getDriver().getTitle(), "Careers | Fortress Information Security"); //verify career page opened
        System.out.println("TEST: [chrome] " + "\tFortressCareerPage1: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
        Reporter.log("TEST: [chrome] " + "\tFortressCareerPage1: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
    }

	/**
	 * FortressHomeJobsPage1: DEPENDS_ON -Navigate to Fortressinfosec.com/careers, find and click /jobs link then verify URL
	 */

    @Test
    public void FortressJobsPage1() throws InterruptedException {
    	FortressCareerPage1(); //go to homepage
        System.out.println("TEST: [chrome] " + "\tFortressJobsPage1: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
        getDriver().findElement(By.cssSelector("#rcc-confirm-button")).click(); //click on career link
        getDriver().findElement(By.xpath("(//a[text()='Click here to join the Team'])[1]")).click(); //click on jobs link
        Thread.sleep(2000);
        //System.out.println("TEST: [chrome] " + "\tFortressJobsPage1: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " + Thread.currentThread().getId());
        Assert.assertEquals(getDriver().getTitle(), "Job Search | Fortress Information Security");
        System.out.println("TEST: [chrome] " + "\tFortressJobsPage1: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
        Reporter.log("TEST: [chrome] " + "\tFortressJobsPage1: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
    }

	/**
	 * JWatchLogInTest1: DEPENDS_ON - Navigate to https://www.jwatch.org/user/login, find and fill login username, 
	 * find and fill login password, find and click on sign-in button then verify correct URL
	 */

    @Test
    public void JWatchLogInTest1() {
        System.out.println("TEST: [chrome] " + "\tJWatchLogInTest1: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
        getDriver().navigate().to("https://www.jwatch.org/user/login");
        getDriver().findElement(By.cssSelector(".main-content #email_text")).sendKeys("neo1491@msn.com");
        getDriver().findElement(By.cssSelector(".main-content #pwd_text")).sendKeys("FortressQA");
        JavascriptExecutor executor = (JavascriptExecutor)getDriver();
        executor.executeScript("$('.main-content .btn-wrapper')[0].click()");
        //System.out.println("TEST: [chrome] " + "\tJWatchLogInTest1: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " +  Thread.currentThread().getId());
        Assert.assertEquals(getDriver().getTitle(), "NEJM Journal Watch");
        System.out.println("TEST: [chrome] " + "\tJWatchLogInTest1: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
        Reporter.log("TEST: [chrome] " + "\tJWatchLogInTest1: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
    }

	/**
	 * DisneyWorldLogInTest1: DEPENDS_ON - Navigate to https://disneyworld.disney.go.com/login, find and fill login username, 
	 * find and fill login password, find and click on sign-in button then verify correct URL
	 */

    @Test
    public void DisneyWorldLogInTest1() {
        System.out.println("TEST: [chrome] " + "\tDisneyWorldLogInTest1: " + "Test Started " + "Thread Id: " +  Thread.currentThread().getId());
        getDriver().navigate().to("https://disneyworld.disney.go.com/login");
        getDriver().findElement(By.cssSelector("[type='email']")).sendKeys("neo1491@msn.com");
        getDriver().findElement(By.cssSelector("[type='password']")).sendKeys("Fortre$$QA");
        getDriver().findElement(By.cssSelector("[type='submit']")).click();
        System.out.println("TEST: [chrome] " + "\tDisneyWorldLogInTest1: " + "Page title: " + getDriver().getTitle() +" " + " Thread Id: " +  Thread.currentThread().getId());
        Assert.assertEquals(getDriver().getTitle(), "Walt Disney World Resort in Orlando, Florida");
        System.out.println("TEST: [chrome] " + "\tDisneyWorldLogInTest1: " + "Test Ended " + "Thread Id: " +  Thread.currentThread().getId());
        Reporter.log("TEST: [chrome] " + "\tDisneyWorldLogInTest1: " + "Test Success " + "Thread Id: " +  Thread.currentThread().getId());
    }
}