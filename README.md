Technology Used

	1. Java
	2. Maven
	3. TestNg
	4. Selenium
	5. Selenium Grid
	6. Rest Assured
	
Installation/Setup

	Localhost: Java >= 1.8, Maven, Eclipse, TestNG and Selenium Grid are required

	Gitlab: Gitlab-ci.yml required
	
Run Tests

	First method:-
	> start selenium grid
	> mvn clean
	> mvn test
	
	Second method:-
	> start selenium grid
	> import the project in Eclipse
	> navigate to project package, right click > Run As > Maven or TestNG Test

	Third method:-
	> run on Gitlab with gitlab-ci.yml
	
	
	
